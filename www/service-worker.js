self.addEventListener("activate", (event) => {
  // console.log("Activate!");
});

// Runs when cache is installed.
self.addEventListener("install", (event) => {
  console.log("Installed service worker.");
});

// Make sure we respond with 200 and cache if offline)
self.addEventListener("fetch", (event) => {
  // console.log("Fetching");
});
