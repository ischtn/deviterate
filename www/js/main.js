const items = ["Product design", "Web development", "UI / UX design"];

const a2hsBar = document.getElementById("a2hs-bar");
const a2hsBtn = document.getElementById("a2hs-button");
let i = 0;

const fadeInOut = () => {
  const tagline = document.getElementById("tagline");

  tagline.innerHTML = items[i];
  i++;
  if (i >= items.length) {
    i = 0;
  }
};

window.setInterval(fadeInOut, 2000);

// Load the webfont
WebFont.load({
  google: {
    families: ["Open Sans:300,400"]
  }
});
